﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEC.YT1300
{
    public sealed class AftCockpit
    {
        public static Enums.PushButton HatchControlSwitch;

        public Enums.Door Hatch()
        {
            switch (HatchControlSwitch)
            {
                case Enums.PushButton.In:
                    return Enums.Door.Open;
                case Enums.PushButton.Out:
                    return Enums.Door.Closed;
            }
            return Hatch();
        }
    }
}
