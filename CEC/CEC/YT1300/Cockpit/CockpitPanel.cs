﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEC.YT1300
{
    public sealed class CockpitPanel
    {
        public static bool isFaulted = false;

        public Enums.Light DistressBeacon()
        {
            if (isFaulted)
                return Enums.Light.On;
            else
                return Enums.Light.Off;
        }

    }
}
