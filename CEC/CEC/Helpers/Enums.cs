﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEC.YT1300
{
    public sealed class Enums
    {
        public enum Light
        {
            Off, On
        }

        public enum PushButton
        {
            Out, In
        }

        public enum Door
        {
            Open, Closed
        }
    }
}
