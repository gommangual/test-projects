﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CEC.YT1300;

namespace YT1300Test
{
    [TestClass]
    public class CockpitTest
    {
        [TestMethod]
        public void DistressBeaconTest()
        {
            CockpitPanel CPanel = new CockpitPanel();

            CockpitPanel.isFaulted = true;

            Assert.AreEqual(Enums.Light.On, CPanel.DistressBeacon());
        }

        [TestMethod]
        public void HatchTest()
        {
            AftCockpit aCockpit = new AftCockpit();

            AftCockpit.HatchControlSwitch = Enums.PushButton.Out;

            Assert.AreEqual(Enums.Door.Closed, aCockpit.Hatch());
        }
    }
}
