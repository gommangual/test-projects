﻿module KnightsOfAether {

    export class Tools {

        public static CheckEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var errorMessage = $('#email-format-error');
           
            if (!regex.test(email)) {
                errorMessage.removeAttr('hidden');
                return false;
            } else {
                errorMessage.attr('hidden', 'true');
                return true;
            }

            //TODO: Check if e-mail exists here...
        }

        public static DoesPasswordMatch(password, confirmation) {
            var password = password.val();
            var confirmPassword = confirmation.val();
            var message = $('#match-error');

            if (password == confirmPassword) {
                message.attr('hidden', 'true');
                return true;
            }
            else {
                message.removeAttr('hidden');
                return false;
            }
        }

        public static CheckPassword(password) {
            //initial strength
            var strength = 0

            //if the password length is less than 6, return message.
            if (password.length < 5) {
                $('#password-help').attr('hidden', 'true');
                $('#password-error').removeAttr('hidden');
                return 'Fail'
            }

            //length is ok, lets continue.

            //if length is 8 characters or more, increase strength value
            if (password.length >= 6) strength += 1

            //if password contains both lower and uppercase characters, increase strength value
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1

            //if it has numbers and characters, increase strength value
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1

            //if it has one special character, increase strength value
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1

            //if it has two special characters, increase strength value
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1

            //now we have calculated strength value, we can return messages

            //if value is less than 2
            if (strength < 4) {
                $('#password-help').attr('hidden', 'true');
                $('#password-error').removeAttr('hidden');
                return 'Fail'
            }
            else {
                $('#password-help').removeAttr('hidden');
                $('#password-error').attr('hidden', 'true');
                return 'Pass'
            }
        }
    }
}