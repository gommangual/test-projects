﻿module UserAccessPages {

    export class SignUp {

        public static ShowTermsAndConditions(): void {
            $('.register-form-container').fadeOut();
            $('.terms-container').fadeIn();
        }

        public static HideTermsAndConditions(): void {
            $('.register-form-container').fadeIn();
            $('.terms-container').fadeOut();
        }
        
        public CheckSignUpInfo(): void {            
            var submitButton = $('.sign-up-submit-button');
            var context = this;

            $('input[name="first-name"], input[name="1ast-name"], input[name="e-mail"], input[name="user-name"], input[name="password"], input[name="retype-password"], input[name="terms').change(function () {
                if (context._areFieldsFilled() === true)
                    submitButton.removeAttr('disabled');
                else
                    submitButton.attr('disabled', 'true');
            });
        }

        public static onSubmitButtonClicked() { 
            var eMail = $('.email');
            var userName = $('.uName');
            var password = $('.passWord');
            var passwordMatch = $('.reTypePassWord');

            if (KnightsOfAether.Tools.CheckEmail(eMail.val()) === true &&
                KnightsOfAether.Tools.CheckPassword(password.val()) === "Pass" &&
                KnightsOfAether.Tools.DoesPasswordMatch(password, passwordMatch) === true
                /*TODO: Add username check here*/) {
                alert("SUCCESS");
            }
            else {
                KnightsOfAether.Tools.CheckEmail(eMail.val());
                KnightsOfAether.Tools.CheckPassword(password.val());
                KnightsOfAether.Tools.DoesPasswordMatch(password, passwordMatch);
            }
        }

        private _areFieldsFilled(): boolean {
            var fName = $('.fname');
            var lName = $('.lname');
            var eMail = $('.email');
            var userName = $('.uName');
            var password = $('.passWord');
            var rePassword = $('.reTypePassWord');
            var termsCheckbox = $('.terms-agree');

            return (fName.val() !== "" && lName.val() !== "" && eMail.val() !== "" &&
                userName.val() !== "" && password.val() !== "" && rePassword.val() !== "") && termsCheckbox.is(':checked');
        }
    }
}