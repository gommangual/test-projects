﻿module KnightsOfAether {

    export class SignIn {

        public Update(): void {
            alert("enters");
        }

        public static Load(): void {
            try {
                //TODO: Add logic here...
            }
            catch (e) {
                //TODO: ..And here
            }

        }

        public static SendVerificationEmail(): void {
            try {
                //TODO: Add logic here...
            }
            catch (e) {
                //TODO: ..And here
            }
        }

        public static ShowUserRecoveryWindow(): void {
            $('.sign-in-form-container').fadeOut();
            $('.forgot-user-container').fadeIn();
        }

        public static HideUserRecoveryWindow(): void {
            $('.sign-in-form-container').fadeIn();
            $('.forgot-user-container').fadeOut();
        }

        public static ShowUserRecoveryPrompt(): void {            
            $('.forgot-user-container').fadeOut();
            $('.sign-in-form-container').fadeIn();
        }
    }
}