var KnightsOfAether;
(function (KnightsOfAether) {
    /** Keeps all the constant variables */
    var Constants = (function () {
        function Constants() {
        }
        //The paths
        Constants.Paths = {
            userAccessPage: "./Source/UserAccess/useraccess.html",
            SignUpPage: "./Source/UserAccess/SignUp/signup.html",
            SignInPage: "./Source/UserAccess/SignIn/signin.html"
        };
        return Constants;
    }());
    KnightsOfAether.Constants = Constants;
})(KnightsOfAether || (KnightsOfAether = {}));
var KnightsOfAether;
(function (KnightsOfAether) {
    var Tools = (function () {
        function Tools() {
        }
        Tools.CheckEmail = function (email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var errorMessage = $('#email-format-error');
            if (!regex.test(email)) {
                errorMessage.removeAttr('hidden');
                return false;
            }
            else {
                errorMessage.attr('hidden', 'true');
                return true;
            }
            //TODO: Check if e-mail exists here...
        };
        Tools.DoesPasswordMatch = function (password, confirmation) {
            var password = password.val();
            var confirmPassword = confirmation.val();
            var message = $('#match-error');
            if (password == confirmPassword) {
                message.attr('hidden', 'true');
                return true;
            }
            else {
                message.removeAttr('hidden');
                return false;
            }
        };
        Tools.CheckPassword = function (password) {
            //initial strength
            var strength = 0;
            //if the password length is less than 6, return message.
            if (password.length < 5) {
                $('#password-help').attr('hidden', 'true');
                $('#password-error').removeAttr('hidden');
                return 'Fail';
            }
            //length is ok, lets continue.
            //if length is 8 characters or more, increase strength value
            if (password.length >= 6)
                strength += 1;
            //if password contains both lower and uppercase characters, increase strength value
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                strength += 1;
            //if it has numbers and characters, increase strength value
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                strength += 1;
            //if it has one special character, increase strength value
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                strength += 1;
            //if it has two special characters, increase strength value
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
                strength += 1;
            //now we have calculated strength value, we can return messages
            //if value is less than 2
            if (strength < 4) {
                $('#password-help').attr('hidden', 'true');
                $('#password-error').removeAttr('hidden');
                return 'Fail';
            }
            else {
                $('#password-help').removeAttr('hidden');
                $('#password-error').attr('hidden', 'true');
                return 'Pass';
            }
        };
        return Tools;
    }());
    KnightsOfAether.Tools = Tools;
})(KnightsOfAether || (KnightsOfAether = {}));
var KnightsOfAether;
(function (KnightsOfAether) {
    /** The main class for the web site */
    var Home = (function () {
        function Home() {
        }
        /** Loads all the .html pages */
        Home.prototype.Load = function () {
            $('#content').empty();
            jQuery.ajaxSetup({ async: false });
            $.get("./Source/Home/home.html", 'html', function (data) { $("#content").append(data); });
            $.get("./Source/Home/Menu/menu.html", 'html', function (data) { $("#content").append(data); });
            $.get("./Source/Home/News/news.html", 'html', function (data) { $("#content").append(data); });
            $.get("./Source/Home/Updates/updates.html", 'html', function (data) { $("#content").append(data); });
            jQuery.ajaxSetup({ async: true });
        };
        Home.OnSignInClicked = function () {
            Home.getUserAccessAdditivePage("Sign In", KnightsOfAether.Constants.Paths.SignInPage);
        };
        Home.OnSignUpClicked = function () {
            Home.getUserAccessAdditivePage("Sign Up", KnightsOfAether.Constants.Paths.SignUpPage);
        };
        /**
         * Adds an additive html page to the main User Access Page
         * @param title: the title of the additive page
         * @param additivePath: the path for the additive page
         */
        Home.getUserAccessAdditivePage = function (title, additivePath) {
            $('#content').empty();
            jQuery.ajaxSetup({ async: false });
            $.get(KnightsOfAether.Constants.Paths.userAccessPage, 'html', function (data) { $("#content").append(data); });
            $.get(additivePath, 'html', function (data) {
                $("#user-access-content").append(data);
                $("#user-access-header-text").html(title);
            });
            jQuery.ajaxSetup({ async: true });
        };
        return Home;
    }());
    KnightsOfAether.Home = Home;
})(KnightsOfAether || (KnightsOfAether = {}));
window.onload = function () {
    var home = new KnightsOfAether.Home();
    home.Load();
};
var News;
(function (News) {
    /**
    * Contains all the patient data
    */
    var BulletinBoard = (function () {
        /**
        * Ctor
        */
        function BulletinBoard(info) {
            //Stores author info
            this.author = "";
            //Stores body info
            this.body = "";
            if (info === null)
                return;
            this.author = info.author;
            this.body = info.body;
        }
        Object.defineProperty(BulletinBoard.prototype, "AUTHOR", {
            /**
            * Gets a string value for AUTHOR
            */
            get: function () {
                return this.author;
            },
            /**
            * Sets a string value for AUTHOR
            */
            set: function (value) {
                this.author = value;
            },
            enumerable: true,
            configurable: true
        });
        ;
        ;
        Object.defineProperty(BulletinBoard.prototype, "BODY", {
            /**
            * Gets a string value for AUTHOR
            */
            get: function () {
                return this.body;
            },
            /**
            * Sets a string value for AUTHOR
            */
            set: function (value) {
                this.body = value;
            },
            enumerable: true,
            configurable: true
        });
        ;
        ;
        return BulletinBoard;
    }());
    News.BulletinBoard = BulletinBoard;
    /**
    * The instance used to populate the news bulletin window with the user updates
    */
    News.BulletinInfo = [];
    function Load() {
        var newInstance = $('#news-template').clone(true, true);
        newInstance.attr('style', '');
        $('#news-list').append(newInstance);
        var newsRowIndex = $("#news-list").children().length - 2;
        var rowId = 'news-row-' + newsRowIndex;
        newInstance.attr('id', rowId);
        var $row = $("#" + rowId);
        var userInfo = News.BulletinInfo[newsRowIndex] ? News.BulletinInfo[newsRowIndex] : AddNewEntry();
        userInfo.rowElement = newInstance.get(0);
        $row.find(".news-author").html(userInfo.AUTHOR);
        $row.find(".news-body").html(userInfo.BODY);
    }
    News.Load = Load;
    /**
    * Adds a new entry to the bulletin board
    */
    function AddNewEntry() {
        var userInfo = new BulletinBoard(null);
        News.BulletinInfo.push(userInfo);
        return userInfo;
    }
    News.AddNewEntry = AddNewEntry;
    /**
    * Deletes a news article from the bulletin list
    * @param rowIndex: deletes the patient row by index (its 0-based)
    */
    function DeleteRow(rowIndex) {
        var foundIndex = false;
        for (var index = 0; index <= BulletinBoard.length; index++) {
            var rowId = $('#' + 'news-row-' + index);
            if (rowIndex === index) {
                foundIndex = true;
                rowId.remove();
                continue;
            }
            var indexId = foundIndex ? index - 1 : index;
            rowId.attr('id', 'news-row-' + index);
        }
        News.BulletinInfo.splice(rowIndex, 1);
    }
    News.DeleteRow = DeleteRow;
    /**
    * Deletes all
    * the news article from the bulletin board
    */
    function DeleteAllData() {
        var $row = $('#news-template').siblings();
        for (var indx = 0; indx < $row.length; indx++) {
            $row[indx].remove();
        }
    }
    News.DeleteAllData = DeleteAllData;
})(News || (News = {}));
var KnightsOfAether;
(function (KnightsOfAether) {
    var SignIn = (function () {
        function SignIn() {
        }
        SignIn.prototype.Update = function () {
            alert("enters");
        };
        SignIn.Load = function () {
            try {
            }
            catch (e) {
            }
        };
        SignIn.SendVerificationEmail = function () {
            try {
            }
            catch (e) {
            }
        };
        SignIn.ShowUserRecoveryWindow = function () {
            $('.sign-in-form-container').fadeOut();
            $('.forgot-user-container').fadeIn();
        };
        SignIn.HideUserRecoveryWindow = function () {
            $('.sign-in-form-container').fadeIn();
            $('.forgot-user-container').fadeOut();
        };
        SignIn.ShowUserRecoveryPrompt = function () {
            $('.forgot-user-container').fadeOut();
            $('.sign-in-form-container').fadeIn();
        };
        return SignIn;
    }());
    KnightsOfAether.SignIn = SignIn;
})(KnightsOfAether || (KnightsOfAether = {}));
var UserAccessPages;
(function (UserAccessPages) {
    var SignUp = (function () {
        function SignUp() {
        }
        SignUp.ShowTermsAndConditions = function () {
            $('.register-form-container').fadeOut();
            $('.terms-container').fadeIn();
        };
        SignUp.HideTermsAndConditions = function () {
            $('.register-form-container').fadeIn();
            $('.terms-container').fadeOut();
        };
        SignUp.prototype.CheckSignUpInfo = function () {
            var submitButton = $('.sign-up-submit-button');
            var context = this;
            $('input[name="first-name"], input[name="1ast-name"], input[name="e-mail"], input[name="user-name"], input[name="password"], input[name="retype-password"], input[name="terms').change(function () {
                if (context._areFieldsFilled() === true)
                    submitButton.removeAttr('disabled');
                else
                    submitButton.attr('disabled', 'true');
            });
        };
        SignUp.onSubmitButtonClicked = function () {
            var eMail = $('.email');
            var userName = $('.uName');
            var password = $('.passWord');
            var passwordMatch = $('.reTypePassWord');
            if (KnightsOfAether.Tools.CheckEmail(eMail.val()) === true &&
                KnightsOfAether.Tools.CheckPassword(password.val()) === "Pass" &&
                KnightsOfAether.Tools.DoesPasswordMatch(password, passwordMatch) === true) {
                alert("SUCCESS");
            }
            else {
                KnightsOfAether.Tools.CheckEmail(eMail.val());
                KnightsOfAether.Tools.CheckPassword(password.val());
                KnightsOfAether.Tools.DoesPasswordMatch(password, passwordMatch);
            }
        };
        SignUp.prototype._areFieldsFilled = function () {
            var fName = $('.fname');
            var lName = $('.lname');
            var eMail = $('.email');
            var userName = $('.uName');
            var password = $('.passWord');
            var rePassword = $('.reTypePassWord');
            var termsCheckbox = $('.terms-agree');
            return (fName.val() !== "" && lName.val() !== "" && eMail.val() !== "" &&
                userName.val() !== "" && password.val() !== "" && rePassword.val() !== "") && termsCheckbox.is(':checked');
        };
        return SignUp;
    }());
    UserAccessPages.SignUp = SignUp;
})(UserAccessPages || (UserAccessPages = {}));
//# sourceMappingURL=KnightsOfAether.js.map