﻿using UnityEngine;

public class ChangeCursor : MonoBehaviour {
    public Texture2D cursorTexture;
    public CursorMode cursorMode;

    public void OnMouseEnter()
    {
        Cursor.SetCursor(cursorTexture, Vector2.zero, cursorMode);
    }
    public void OnMouseExit()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }

}
