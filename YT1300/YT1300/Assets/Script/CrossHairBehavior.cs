﻿
using UnityEngine;
using UnityEngine.UI;

public class CrossHairBehavior: MonoBehaviour
{
    public void Start()
    {
        EventManager.StartListening("InteracteableItemFound", _onFocusIn);
        EventManager.StartListening("InteracteableItemNotFound", _onFocusOut);
    }

    private void _onFocusIn()
    {
        GetComponent<Image>().color = Color.red;        
    }

    private void _onFocusOut()
    {
        GetComponent<Image>().color = Color.white;
    }
}
