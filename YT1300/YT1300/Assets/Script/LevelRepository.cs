﻿using Assets.Script;
using UnityEngine;
using UnityEngine.UI;

public class LevelRepository: MonoBehaviour
{
    // Assign this with the prefab gameobject that has an Image component in the inspector.
    public Image imagePrefab;

    public LevelAttributes[] _levels;
    private GameObject _parent = null;

    public void Start()
    {
        _parent = GameObject.Find("LevelWindow");

        for (int index=0; index < _levels.Length; index++)
        {
            Image instance = Instantiate<Image>(imagePrefab);

            instance.transform.localPosition = Vector3.zero;
            instance.transform.localScale = Vector3.one;
            instance.transform.SetParent(_parent.transform, false);

            instance.GetComponent<Image>().sprite = _levels[index].image;            
            instance.GetComponentInChildren<Text>().text = _levels[index].title;

            if (_levels[index].isActive)
                instance.GetComponent<Image>().color = Color.white;
            else
            {
                instance.GetComponent<Image>().color = Color.gray;
                instance.GetComponent<Button>().enabled = false;
            } 
        }        
    }
}
