﻿using System.Collections;
using UnityEngine;

public class PlayerRestrictor : MonoBehaviour {
    public GameObject target;
    public GameObject UICamera;
    public GameObject PerspectiveCamera;
    public GameObject popup;

    void Start()
    {
        EventManager.StartListening("DoneRotatingPlayer", _finishedRotating);
    }
    void OnTriggerEnter(Collider collision)
    {
        EventManager.TriggerEvent("PausePlayer"); 
        StartCoroutine(RotateMe(target.transform, 2));
    }

    IEnumerator RotateMe(Transform player, float inTime)
    {
        var fromAngle = player.rotation;
        var toAngle = Quaternion.Euler(0, 90, 0);
        for (var t = 0f; t < 1; t += Time.deltaTime / inTime)
        {
            player.rotation = Quaternion.Lerp(fromAngle, toAngle, t);
            yield return null;
        }
        EventManager.TriggerEvent("DoneRotatingPlayer");
    }

    void _finishedRotating()
    {
        PerspectiveCamera.SetActive(false);
        UICamera.SetActive(true);
        popup.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
}
