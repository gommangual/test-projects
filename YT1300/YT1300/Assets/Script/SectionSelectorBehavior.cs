﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SectionSelectorBehavior : MonoBehaviour {
    public void onMouseEnter()
    {
        EventManager.TriggerEvent(name+"AreaHovered");
    }

    public void onMouseExit()
    {
        EventManager.TriggerEvent(name+"AreaExited");
    }
    public void onMouseClick()
    {
        EventManager.TriggerEvent(name + "AreaClicked");
    }
    void Start()
    {
        EventManager.StartListening(name+"AreaHovered", _areaHovered);
        EventManager.StartListening(name+"AreaExited", _areaExited);
    }

    private void _areaHovered()
    {
        _setFontColor("#A6A800FF");
        _setFontSize(36);
    }

    private void _areaExited()
    {
        _setFontColor("#FBFF00FF");
        _setFontSize(32);
    }

    private void _setFontColor(string hexColor)
    {
        try
        {
            Color color = new Color();
            ColorUtility.TryParseHtmlString(hexColor, out color);

            GetComponent<Text>().color = color;
        }
        catch(Exception e)
        {
            Debug.Log("Error: Message: " + e.Message);
        }
    }

    private void _setFontSize(int size)
    {
        try
        {
            GetComponent<Text>().fontSize = size;

        }
        catch (Exception e)
        {
            Debug.Log("Message: " + e.Message);
        }
    }
}
