﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MenuItemBehavior : MonoBehaviour {
    
    public void OnMouseClick()
    {
        try
        {
            if (GetComponent<Button>().IsInteractable())
            {
                if(gameObject.name == "Exit")
                {
                    Application.Quit();
                }
            }               
        }
        catch(Exception e)
        {
            Debug.Log("Error: Message: " + e.Message);
        }        
    }

    public void OnMouseEnter()
    {
        try
        {
            Color color = new Color(0.618f, 0.574f, 0, 1);

            if (GetComponent<Button>().IsInteractable())
            {
                GetComponent<Text>().color = color;
                gameObject.GetComponentInChildren<Image>().enabled = true;
            }                
        }
        catch (Exception e)
        {
            Debug.Log("Error: Message: " + e.Message);
        }
    }

    public void OnMousExit()
    {
        try
        {
            Color color = new Color(255, 237, 0, 1);

            if (GetComponent<Button>().IsInteractable())
            {
                GetComponent<Text>().color = color;
                gameObject.GetComponentInChildren<Image>().enabled = false;
            }                
        }
        catch (Exception e)
        {
            Debug.Log("Error: Message: " + e.Message);
        }
    }
}
