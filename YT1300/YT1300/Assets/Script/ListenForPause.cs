﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class ListenForPause: MonoBehaviour
{
    void Start()
    {
        EventManager.StartListening("PausePlayer", _pausePlayer);
        EventManager.StartListening("UnPausePlayer", _unPausePlayer);
    }

    void _pausePlayer()
    {
        GetComponent<FirstPersonController>().enabled = false;
    }
    void _unPausePlayer()
    {
        GetComponent<FirstPersonController>().enabled = true;
    }
}
