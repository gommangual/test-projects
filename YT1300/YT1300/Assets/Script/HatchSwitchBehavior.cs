﻿using UnityEngine;

public class HatchSwitchBehavior: MonoBehaviour
{
    private void OnMouseUpAsButton()
    {
        EventManager.TriggerEvent("ToggleHatch");               
    }

}
