﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class LoadLevelByName : MonoBehaviour {

    public string sceneName;

    public void onLevelClicked()
    {
        try
        {
            SceneManager.LoadScene(sceneName);
        }
        catch (Exception e)
        {

            Debug.Log("Message: " + e.Message);
        }

    }
}
