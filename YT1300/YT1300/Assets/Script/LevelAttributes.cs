﻿using System;
using UnityEngine;

namespace Assets.Script
{
    [Serializable]
    public class LevelAttributes
    {
        public Sprite image;
        public string title;
        public bool isActive;
    }
}
