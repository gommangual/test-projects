﻿using UnityEngine;

public class InteracteableItemFinder : MonoBehaviour {
    // Update is called once per frame
    void OnGUI()
    {
        var forward = transform.TransformDirection(Vector3.forward);
        //note the use of var as the type. This is because in c# you 
        // can have lamda functions which open up the use of untyped variables
        //these variables can only live INSIDE a function. 
        RaycastHit hit;

        Debug.DrawRay(transform.position, forward * 2, Color.red);

        if (Physics.Raycast(transform.position, forward, out hit, 3) && hit.collider.gameObject.tag == "Interacteable")
        {
            EventManager.TriggerEvent("InteracteableItemFound");
        }
        else
        {
            EventManager.TriggerEvent("InteracteableItemNotFound");
        }
    }
    
}
