﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityStandardAssets.Characters.FirstPerson;

public class MapItemBehavior : MonoBehaviour {
    public GameObject MapSource;
    public Sprite SelectedArea;
    public GameObject Tooltip;
    public GameObject SpawnPoint;
    public GameObject Player;
    public GameObject PopUp;
    public Sprite originalMap;
    public GameObject SpawnPointList;

    public void onMouseEnter()
    {
        EventManager.TriggerEvent(name+"AreaHovered");
    }

    public void onMouseExit()
    {
        EventManager.TriggerEvent(name + "AreaExited");
    }
    public void onMouseClick()
    {
        EventManager.TriggerEvent(name + "AreaClicked");
    }
    void Start()
    {
        EventManager.StartListening(name +"AreaHovered", _onAreaMouseHover);
        EventManager.StartListening(name + "AreaExited", _onAreaMouseExit);
        EventManager.StartListening(name + "AreaClicked", _onAreaMouseClick);
    }

    private void _onAreaMouseHover()
    {
        MapSource.GetComponent<Image>().sprite = SelectedArea;
        Tooltip.GetComponent<Image>().enabled = true;
        Tooltip.GetComponentInChildren<Text>().enabled = true;
    }

    private void _onAreaMouseExit()
    {
        MapSource.GetComponent<Image>().sprite = originalMap;
        Tooltip.GetComponent<Image>().enabled = false;
        Tooltip.GetComponentInChildren<Text>().enabled = false;
    }

    private void _onAreaMouseClick()
    {
        try
        {
            Player.gameObject.SetActive(true);
            Player.GetComponent<FirstPersonController>().enabled = true;
            Player.transform.position = SpawnPoint.transform.position;

            GameObject.Find("MainMenu").SetActive(false);
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

            SpawnPointList.SetActive(false);
            EventManager.TriggerEvent("EnterShip");
        }
        catch(Exception e)
        {
            if (PopUp != null) PopUp.SetActive(true);
            Debug.Log("Error: Message: " + e.Message);
        }
    }
}
