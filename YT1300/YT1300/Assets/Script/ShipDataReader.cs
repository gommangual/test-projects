﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ShipDataReader: MonoBehaviour {
    public CEC.Enums.LevelNames LevelName;
    public GameObject  Title;
    public GameObject  AreaPrefab;
    public GameObject  AreaRoot;
    public GameObject Description;

    void Start()
    {
        string filepath = Resources.Load("Ships").ToString();
        JSONData Data = JsonUtility.FromJson<JSONData>(filepath);
        
        Title.GetComponentInChildren<Text>().text = Data.Scenes[(int)LevelName].Name;
        Description.GetComponentInChildren<Text>().text = Data.Scenes[(int)LevelName].Description;

        for (int areaIndex=0; areaIndex < Data.Scenes[(int)LevelName].Areas.Length; areaIndex++)
        {
            GameObject instance = Instantiate(AreaPrefab);
            instance.name = Data.Scenes[(int)LevelName].Areas[areaIndex].Name;
            instance.transform.SetParent(AreaRoot.transform, false);
            instance.GetComponentInChildren<Text>().text = Data.Scenes[(int)LevelName].Areas[areaIndex].Title;
        }
    }
}

/// <summary>
/// Outer Structure
/// </summary>
[Serializable]
public class JSONData
{
    public Scenes[] Scenes;
}

/// <summary>
/// Inner Structure
/// </summary>
[Serializable]
public class Scenes
{
    public string Name;
    public string Title;
    public string SourceImage;
    public string Description;
    public Areas[] Areas;
}

/// <summary>
/// Outer Structure
/// </summary>
[Serializable]
public class Areas
{
    public string Name;
    public string Title;
}
