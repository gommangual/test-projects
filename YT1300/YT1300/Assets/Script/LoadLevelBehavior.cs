﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadLevelBehavior: MonoBehaviour
{
    public void onLevelClicked()
    {
        try
        {
            SceneManager.LoadScene(GetComponentInChildren<Text>().text);
        }
        catch (Exception e)
        {

            Debug.Log("Message: " + e.Message);
        }
        
    }
}
