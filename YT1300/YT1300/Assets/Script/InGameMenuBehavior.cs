﻿using UnityEngine;

public class InGameMenuBehavior : MonoBehaviour {
    public GameObject UICamera;
    public GameObject InGameMenu;
    public GameObject perspectiveCamera;
    public GameObject areaSelectionCanvas;
    public GameObject spawnPoints;
    public GameObject crossHair;

    private bool _enteredShip = false;
    private bool _playerPaused = false;

    void Start()
    {
        EventManager.StartListening("EnterShip", _onEnterShip);
        EventManager.StartListening("ReturnClicked", _onReturn);
        EventManager.StartListening("MapClicked", _onMap);
        EventManager.StartListening("ObjectivesClicked", _onObjectives);
        EventManager.StartListening("OptionsClicked", _onOptions);
        EventManager.StartListening("MainMenuClicked", _onExitShip);
        EventManager.StartListening("PausePlayer", _isPlayerPaused);
        EventManager.StartListening("UnPausePlayer", _isPlayerUnPaused);
    }
    void Update()
    {
        if (_enteredShip && !_playerPaused && Input.GetKeyUp(KeyCode.Escape))
        {
            _inGameMenuBehavior();
        }
    }
    private void _onEnterShip()
    {
        _enteredShip = true;
        Cursor.lockState = CursorLockMode.Locked;
        UICamera.SetActive(true);
        crossHair.SetActive(true);
    }
    private void _isPlayerPaused()
    {
        _playerPaused = true;
        crossHair.SetActive(false);
    }
    public void _isPlayerUnPaused()
    {
        _playerPaused = false;
        crossHair.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;
    }
    private void _onReturn()
    {
        InGameMenu.SetActive(false);
        EventManager.TriggerEvent("UnPausePlayer");
    }
    private void _onMap()
    {
        //TODO: Add logic here...
    }
    private void _onObjectives()
    {
        //TODO: Add logic here...
    }
    private void _onOptions()
    {
        //TODO: Add logic here...   
    }
    private void _onExitShip()
    {
        UICamera.SetActive(false);
        InGameMenu.SetActive(false);
        perspectiveCamera.SetActive(true);
        areaSelectionCanvas.SetActive(true);
        spawnPoints.SetActive(true);
        EventManager.TriggerEvent("PausePlayer");
        _playerPaused = false;
    }
    private void _inGameMenuBehavior()
    {
        UICamera.SetActive(true);
        InGameMenu.SetActive(true);
        perspectiveCamera.SetActive(false);
        EventManager.TriggerEvent("PausePlayer");
        Cursor.visible = true;
    }    
}
