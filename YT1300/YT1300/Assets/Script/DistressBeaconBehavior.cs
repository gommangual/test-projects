﻿using System.Collections;
using UnityEngine;
using CEC.YT1300;

public class DistressBeaconBehavior: MonoBehaviour
{
    Renderer rend;
    Material mat;
    public Color colorStart;
    public Color colorEnd;
    private float duration = 1.0F;
    CockpitPanel CPanel = new CockpitPanel();

    public void Start()
    {
        rend = GetComponent<Renderer>();
        mat = rend.material;

        //**********************************************************
        CockpitPanel.isFaulted = true; //This is for testing purposes only, it does NOT belong here
        //**********************************************************

        gameObject.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");

        if (CPanel.DistressBeacon() == Enums.Light.On)
            StartCoroutine(FlashLight());
        else
            StopCoroutine(FlashLight());
    }

    IEnumerator FlashLight()
    {
        while (true)
        {
            float lerp = Mathf.PingPong(Time.time, duration) / duration;
            yield return new WaitForSeconds(1);
            mat.SetColor("_EmissionColor", Color.Lerp(colorStart, colorEnd, lerp));
            yield return new WaitForSeconds(1);
            mat.SetColor("_EmissionColor", Color.Lerp(colorEnd, colorStart, lerp));
        }        
    }
}
