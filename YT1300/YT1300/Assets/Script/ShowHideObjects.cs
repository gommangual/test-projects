﻿using UnityEngine;

public class ShowHideObjects : MonoBehaviour {

    public GameObject[] Disable;
    public GameObject[] Enable;

    // Use this for initialization
    public void onItemClicked () {
        if (Disable != null)
        {
            for (int i = 0; i < Disable.Length; i++)
            {
                Disable[i].SetActive(false);
            }
        }
        if (Enable != null)
        {
            for (int i = 0; i < Enable.Length; i++)
            {
                Enable[i].SetActive(true);
            }
        }
	}
}
