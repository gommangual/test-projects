﻿using UnityEngine;

class EventTrigger : MonoBehaviour
{
    public void onClick()
    {
        EventManager.TriggerEvent(name + "Clicked");
    }
}
