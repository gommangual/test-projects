﻿using CEC.YT1300;
using UnityEngine;

public class CockpitDoorBehavior : MonoBehaviour {
    private static int toggleCounter = 0;

    // Use this for initialization
    void Start () {
        toggleCounter = (int)AftCockpit.HatchControlSwitch;
        EventManager.StartListening("ToggleHatch", _onDoorToggle);
	}
	
	private void _onDoorToggle()
    {
        if(toggleCounter == 0)
        {
            GetComponent<Animation>().Play("OpenDoor");
            toggleCounter++;
        }
        else
        {
            GetComponent<Animation>().Play("CloseDoor");
            toggleCounter = 0;
        }
    }
}
