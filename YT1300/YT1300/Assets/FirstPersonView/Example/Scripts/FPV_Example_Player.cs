﻿using UnityEngine;

namespace FirstPersonViewer.Example
{
    public class FPV_Example_Player : MonoBehaviour
    {
        [Header("Cameras")]
        public Camera worldCamera;

        void Update()
        {
            ChangeFOV();
        }

        private void ChangeFOV()
        {
            //World Camera FOV
            float fOVChange = 0;
            if(Input.GetKey(KeyCode.Comma))
            {
                fOVChange = -Time.deltaTime*10;
            }
            else if(Input.GetKey(KeyCode.Period))
            {
                fOVChange = Time.deltaTime*10;
            }
            worldCamera.fieldOfView = Mathf.Clamp(worldCamera.fieldOfView + fOVChange, 50, 120);

            //First Person Camera FOV
            fOVChange = 0;
            if (Input.GetKey(KeyCode.N))
            {
                fOVChange = -Time.deltaTime*10;
            }
            else if (Input.GetKey(KeyCode.M))
            {
                fOVChange = Time.deltaTime*10;
            }
        }
    }
}
